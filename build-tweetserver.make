; Tweet Server
; -------------

; Core version
; -------------
core = 7.x

; API version
; ------------
api = 2

; Drupal core
; -------------
projects[] = drupal

; Defaults
; ---------
defaults[projects][subdir] = tweetserver

; Contrib projects
; -----------------
projects[] = composer_autoload
projects[] = devel
projects[] = guzzle
projects[] = twitter_api
